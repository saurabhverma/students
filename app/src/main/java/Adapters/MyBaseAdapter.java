package Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.clicklabs124.students.R;

import java.util.ArrayList;

import entities.Student;

public class MyBaseAdapter extends BaseAdapter {
    ArrayList<Student> studentList = new ArrayList<Student>();

    LayoutInflater inflater;
    Context context;

    public MyBaseAdapter(Context context, ArrayList<Student> myList) {
        this.studentList = myList;
        this.context = context;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return studentList.size();
    }

    @Override
    public Student getItem(int position) {
        return studentList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder mViewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.listview_each_item, parent, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        Student studentData = getItem(position);

        mViewHolder.roll.setText(String.valueOf(studentData.getRoll()));
        mViewHolder.name.setText(studentData.getName());
        String imagePath = studentData.getImagePath();
        if (imagePath == null) {
            mViewHolder.image.setImageResource(R.mipmap.profile);
        } else {
            //String image = studentData.getImagePath();
            mViewHolder.image.setImageBitmap(getRoundedShape(BitmapFactory.decodeFile(studentData.getImagePath()), 150));
        }

        return convertView;
    }

    private class MyViewHolder {
        TextView roll, name;
        ImageView image;

        public MyViewHolder(View item) {
            roll = (TextView) item.findViewById(R.id.roll);
            name = (TextView) item.findViewById(R.id.name);
            image = (ImageView) item.findViewById(R.id.image);

        }
    }

    public static Bitmap getRoundedShape(Bitmap scaleBitmapImage, int width) {
        // TODO Auto-generated method stub
        int targetWidth = width;
        int targetHeight = width;
        Bitmap targetBitmap = Bitmap.createBitmap(targetWidth,
                targetHeight, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(targetBitmap);
        Path path = new Path();
        path.addCircle(((float) targetWidth - 1) / 2,
                ((float) targetHeight - 1) / 2,
                (Math.min(((float) targetWidth),
                        ((float) targetHeight)) / 2),
                Path.Direction.CCW);
        canvas.clipPath(path);
        Bitmap sourceBitmap = scaleBitmapImage;
        canvas.drawBitmap(sourceBitmap,
                new Rect(0, 0, sourceBitmap.getWidth(),
                        sourceBitmap.getHeight()),
                new Rect(0, 0, targetWidth,
                        targetHeight), null);
        return targetBitmap;
    }
}
