package Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import entities.Student;

import java.lang.*;
import java.util.ArrayList;

public class StudentDatabase extends SQLiteOpenHelper {

    public static int dbVersion = 1;
    public static final String DATABASE_NAME = "StudentDatabase.db";
    public static final String TABLE_NAME = "studentDetails";
    public static final String STUDENT_NAME = "name";
    public static final String STUDENT_ROLL = "roll";
    public static final String STUDENT_IMAGE = "image";


    public StudentDatabase(Context context) {
        super(context, DATABASE_NAME, null, dbVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table studentDetails " +
                        "(roll integer primary key, name text,image text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS studentDetails");
        onCreate(db);
    }

    public boolean insertContact(int roll, String name, String image) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        if (checkData(roll)) {
            contentValues.put(STUDENT_ROLL, roll);
            contentValues.put(STUDENT_NAME, name);
            contentValues.put(STUDENT_IMAGE, image);
            db.insert("studentDetails", null, contentValues);
            return true;
        } else {
            return false;
        }
    }

    public boolean checkData(int roll) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from studentDetails where roll=" + roll + "", null);
        if (res.getCount() <= 0) {
            return true;
        }
        return false;
    }

   /* public int numberOfRows() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, TABLE_NAME);
        return numRows;
    }*/

    public boolean updateContact(Integer oldRoll, Integer roll, String name, String image) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(STUDENT_ROLL, roll);
        contentValues.put(STUDENT_NAME, name);
        contentValues.put(STUDENT_IMAGE, image);
        db.update("studentDetails", contentValues, "roll = ? ", new String[]{String.valueOf(oldRoll)});
        return true;
    }

    public Integer deleteContact(Integer roll) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("studentDetails",
                "roll = ? ",
                new String[]{Integer.toString(roll)});

    }

    public Cursor getAllCotacts() {

        SQLiteDatabase db = this.getReadableDatabase();
        String[] tableColumns = {STUDENT_ROLL, STUDENT_NAME, STUDENT_IMAGE};
        Cursor cursor = db.query(TABLE_NAME, tableColumns, null, null, null, null, null);
        return cursor;
    }

    public ArrayList<Student> getUpdateList(ArrayList<Student> array_list) {
        array_list.clear();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from studentDetails", null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            Student obj = new Student(Integer.parseInt(res.getString(0)), res.getString(1), res.getString(2));
            array_list.add(obj);
            res.moveToNext();
        }
        return array_list;
    }
}
