package Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.clicklabs124.students.ActivityCommunicator;
import com.example.clicklabs124.students.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

;

public class addStudentFragment extends Fragment {

    Button button;
    EditText name;
    EditText rollNo;
    private int LOAD_IMAGE_RESULTS = 1;
    private int REQUEST_TAKE_PHOTO = 3;
    private Button imageButton;
    private ImageView image;
    String studentName;
    String imagePath = null;
    Integer roll, rollToUpdate;
    String studentRoll;
    public Context context;
    private ActivityCommunicator activityCommunicator;
    boolean flag;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.add_student_layout, container, false);
        image = (ImageView) v.findViewById(R.id.image);
        button = (Button) v.findViewById(R.id.addStudent);
        name = (EditText) v.findViewById(R.id.studentName);
        rollNo = (EditText) v.findViewById(R.id.roll);
        imageButton = (Button) v.findViewById(R.id.addImageButton);
        image.setImageResource(R.mipmap.profile);
        addListenerOnButton();
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (flag) {
            rollNo.setText(studentRoll);
            name.setText(studentName);
            if (imagePath == null) {
                image.setImageResource(R.mipmap.profile);
            } else {
                image.setImageBitmap(BitmapFactory.decodeFile(imagePath));
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        context = getActivity();
        activityCommunicator = (ActivityCommunicator) context;

    }


    private void addListenerOnButton() {

        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                studentName = name.getText().toString();
                roll = Integer.parseInt(rollNo.getText().toString());
                Bundle bundle = new Bundle();
                bundle.putInt("roll", roll);
                bundle.putString("name", studentName);
                bundle.putString("imagepath", imagePath);
                bundle.putString("action", "add");
                if (flag) {
                    bundle.putBoolean("edit", true);
                    bundle.putInt("rollToUpdate", rollToUpdate);
                }
                flag = false;
                activityCommunicator.passDataToActivity(bundle);
                name.setText(null);
                rollNo.setText(null);
                image.setImageResource(R.mipmap.profile);
            }

        });
        imageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                selectImage();

            }
        });
    }

    void selectImage() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("Add Profile Image");
        dialog.setItems(R.array.image_menu, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_TAKE_PHOTO);

                } else if (item == 1) {
                    Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, LOAD_IMAGE_RESULTS);
                } else {
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOAD_IMAGE_RESULTS && data != null) {
            Uri pickedImage = data.getData();
            String[] filePath = {MediaStore.Images.Media.DATA};
            Cursor cursor = getActivity().getContentResolver().query(pickedImage, filePath, null, null, null);
            cursor.moveToFirst();
            imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));
            image.setImageBitmap(BitmapFactory.decodeFile(imagePath));
            cursor.close();
        }
        if (requestCode == REQUEST_TAKE_PHOTO) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

            File destination = new File(Environment.getExternalStorageDirectory(),
                    System.currentTimeMillis() + ".jpg");

            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            imagePath = destination.getAbsolutePath();
            image.setImageBitmap(thumbnail);
        }
    }

    public void editData(Bundle bundleOject) {

        flag = true;
        studentName = bundleOject.getString("name");
        rollToUpdate = bundleOject.getInt("roll");
        studentRoll = String.valueOf(rollToUpdate);
        imagePath = bundleOject.getString("imagepath");

        onResume();

    }
}