package Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.clicklabs124.students.ActivityCommunicator;
import com.example.clicklabs124.students.R;
import com.example.clicklabs124.students.ViewStudentDetails;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import Adapters.MyBaseAdapter;
import Database.StudentDatabase;
import entities.Student;
import util.AppConstant;

public class viewStudentFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    Button viewButton;
    public ArrayList<Student> studentData = new ArrayList<Student>();
    String name, imagePath;
    int rollNo;
    MyBaseAdapter adapter;
    ListView listView;
    GridView gridView;
    public Context context;
    private ActivityCommunicator activityCommunicator;
    int menuSelectedItemIndex;
    StudentDatabase database;
    Spinner spinner;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        database = new StudentDatabase(getActivity());
        Cursor cursor = database.getAllCotacts();
        cursor.moveToFirst();
        try {
            do {
                Student studentObject = new Student(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2));
                studentData.add(studentObject);
            } while (cursor.moveToNext());
        } catch (CursorIndexOutOfBoundsException exception) {
            exception.fillInStackTrace();
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.view_student_layout, container, false);
        listView = (ListView) v.findViewById(R.id.studentList);
        adapter = new MyBaseAdapter(getActivity(), studentData);
        listView.setAdapter(adapter);
        gridView = (GridView) v.findViewById(R.id.gridView);
        gridView.setVisibility(View.INVISIBLE);
        viewButton = (Button) v.findViewById(R.id.viewButton);
        registerForContextMenu(listView);
        registerForContextMenu(gridView);
        spinner = (Spinner) v.findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(this);
        List<String> categories = new ArrayList<String>();
        categories.add("Sort");
        categories.add("By Name");
        categories.add("By Roll No.");
        ArrayAdapter<String> dropMenuAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories);
        dropMenuAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dropMenuAdapter);
        addListenerOnButton();
        return v;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();
        if (item.equals("By Name")) {
            sortName();
            Toast.makeText(parent.getContext(), "Sorted: " + item, Toast.LENGTH_SHORT).show();
        } else if (item.equals("By Roll No.")) {
            sortRoll();
            Toast.makeText(parent.getContext(), "Sorted: " + item, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Options");
        menu.add(0, AppConstant.VIEW, 1, "View");//groupId, itemId, order, title
        menu.add(0, AppConstant.EDIT, 2, "Edit");
        menu.add(0, AppConstant.DELETE, 3, "Delete");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        menuSelectedItemIndex = info.position;
        String name = adapter.getItem(info.position).getName();
        Integer roll_To_Update = adapter.getItem(info.position).getRoll();
        String roll = String.valueOf(adapter.getItem(info.position).getRoll());
        String imagePath = adapter.getItem(info.position).getImagePath();
        Intent intent;
        switch (item.getItemId()) {
            case AppConstant.VIEW:
                intent = new Intent(getActivity(), ViewStudentDetails.class);
                intent.putExtra("studentName", name);
                intent.putExtra("roll", roll);
                intent.putExtra("imagePath", imagePath);
                startActivity(intent);
                return true;
            case AppConstant.EDIT:
                Bundle bundle = new Bundle();
                bundle.putInt("roll", Integer.parseInt(roll));
                bundle.putString("name", name);
                bundle.putString("imagepath", imagePath);
                bundle.putInt("rollToUpdate", roll_To_Update);
                bundle.putString("action", "edit");
                activityCommunicator.passDataToActivity(bundle);
                return true;
            case AppConstant.DELETE:
                studentData.remove(info.position);
                database.deleteContact(Integer.parseInt(roll));
                adapter.notifyDataSetChanged();
                return true;
            default:
                return true;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        context = getActivity();
        activityCommunicator = (ActivityCommunicator) context;
    }

    public void addListenerOnButton() {

        viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (viewButton.getText().equals("Grid")) {
                    gridView.setAdapter(adapter);
                    gridView.setVisibility(View.VISIBLE);
                    listView.setVisibility(View.INVISIBLE);
                    viewButton.setText("List");

                } else if (viewButton.getText().equals("List")) {
                    listView.setAdapter(adapter);
                    listView.setVisibility(View.VISIBLE);
                    gridView.setVisibility(View.INVISIBLE);
                    viewButton.setText("Grid");
                }
            }
        });
    }

    private Comparator<Student> byName() {
        return new Comparator<Student>() {
            public int compare(Student o1, Student o2) {
                return o1.name.compareToIgnoreCase(o2.name);
            }
        };
    }

    public void sortName() {
        Collections.sort(studentData, byName());
        for (int i = 0; i < studentData.size(); i++) {
            adapter.notifyDataSetChanged();
        }
    }

    private Comparator<Student> byRoll() {
        return new Comparator<Student>() {
            public int compare(Student o1, Student o2) {
                if (o1.getRoll() > o2.getRoll()) {
                    return 1;
                } else {
                    return -1;
                }
            }
        };
    }

    public void sortRoll() {
        Collections.sort(studentData, byRoll());
        for (int i = 0; i < studentData.size(); i++) {
            adapter.notifyDataSetChanged();
        }
    }


    public void addData(Bundle bundleOject) {

        if (bundleOject.getBoolean("edit")) {
            name = bundleOject.getString("name");
            rollNo = bundleOject.getInt("roll");
            imagePath = bundleOject.getString("imagepath");
            Integer roll_To_Update = bundleOject.getInt("rollToUpdate");
            database.updateContact(roll_To_Update, rollNo, name, imagePath);
            adapter.getItem(menuSelectedItemIndex).setName(name);
            adapter.getItem(menuSelectedItemIndex).setImagePath(imagePath);
            adapter.getItem(menuSelectedItemIndex).setRoll(rollNo);
            Toast.makeText(getActivity(), "Updated Successfully", Toast.LENGTH_SHORT).show();
            adapter.notifyDataSetChanged();
        } else {
            name = bundleOject.getString("name");
            rollNo = bundleOject.getInt("roll");
            imagePath = bundleOject.getString("imagepath");
            Student student = new Student(rollNo, name, imagePath);
            boolean flag = database.insertContact(rollNo, name, imagePath);
            if (flag) {
                studentData.add(student);
                adapter.notifyDataSetChanged();
                Toast.makeText(getActivity().getApplicationContext(), "Saved Successfully", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
