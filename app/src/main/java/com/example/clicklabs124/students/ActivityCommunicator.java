package com.example.clicklabs124.students;

import android.os.Bundle;

import java.util.ArrayList;

public interface ActivityCommunicator {
    public void passDataToActivity(Bundle bundleObject);
}
