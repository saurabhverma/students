package com.example.clicklabs124.students;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabContentFactory;
import java.util.List;
import java.util.Vector;
import Adapters.MyFragmentAdapter;
import Fragments.addStudentFragment;
import Fragments.viewStudentFragment;


public class MainActivity extends FragmentActivity implements
        OnTabChangeListener, OnPageChangeListener, ActivityCommunicator {

    private TabHost mtabHost;
    private ViewPager viewPager;
    private MyFragmentAdapter myViewPagerAdapter;

    class TabContent implements TabContentFactory {
        private final Context mContext;

        public TabContent(Context context) {
            mContext = context;
        }

        @Override
        public View createTabContent(String tag) {
            View v = new View(mContext);
            v.setMinimumHeight(0);
            v.setMinimumWidth(0);
            return v;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initializeTabHost(savedInstanceState);
        this.initializeViewPager();

    }

    private void initializeViewPager() {
        List<Fragment> fragments = new Vector<Fragment>();

        fragments.add(new viewStudentFragment());
        fragments.add(new addStudentFragment());

        this.myViewPagerAdapter = new MyFragmentAdapter(
                getSupportFragmentManager(), fragments);
        this.viewPager = (ViewPager) super.findViewById(R.id.viewPager);
        this.viewPager.setAdapter(this.myViewPagerAdapter);
        this.viewPager.setOnPageChangeListener(this);

        onRestart();

    }

    private void initializeTabHost(Bundle args) {

        mtabHost = (TabHost) findViewById(android.R.id.tabhost);
        mtabHost.setup();

        TabHost.TabSpec tabSpec;
        tabSpec = mtabHost.newTabSpec("viewStudent");
        tabSpec.setIndicator("View Student");
        tabSpec.setContent(new TabContent(this));
        mtabHost.addTab(tabSpec);
        tabSpec = mtabHost.newTabSpec("addStudent");
        tabSpec.setIndicator("Add Student");
        tabSpec.setContent(new TabContent(this));
        mtabHost.addTab(tabSpec);
        mtabHost.setOnTabChangedListener(this);
    }

    @Override
    public void onTabChanged(String tabId) {
        int pos = this.mtabHost.getCurrentTab();
        this.viewPager.setCurrentItem(pos);

        HorizontalScrollView hScrollView = (HorizontalScrollView) findViewById(R.id.hScrollView);
        View tabView = mtabHost.getCurrentTabView();
        int scrollPos = tabView.getLeft()
                - (hScrollView.getWidth() - tabView.getWidth()) / 2;
        hScrollView.smoothScrollTo(scrollPos, 0);

    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
    }

    @Override
    public void onPageSelected(int position) {
        this.mtabHost.setCurrentTab(position);
    }

    public void passDataToActivity(Bundle bundleObject) {
        Bundle bundle = new Bundle(bundleObject);
        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        viewStudentFragment frg = (viewStudentFragment) fm.findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + 0);
        addStudentFragment frg1 = (addStudentFragment) fm.findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + 1);
        if (bundleObject.getString("action").equals("add")) {
            frg.addData(bundle);

            mtabHost.setCurrentTab(0);
        }
        if (bundleObject.getString("action").equals("edit")) {

            frg1.editData(bundle);
            mtabHost.setCurrentTab(1);
        }
    }

}
