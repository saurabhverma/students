package com.example.clicklabs124.students;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;


public class ViewStudentDetails extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_student_details);
        Intent intent = getIntent();
        String studentName = intent.getExtras().getString("studentName");
        String rollNo = intent.getExtras().getString("roll");
        String imagePath = intent.getExtras().getString("imagePath");
        TextView roll = (TextView) findViewById(R.id.roll);
        TextView name = (TextView) findViewById(R.id.name);
        ImageView image = (ImageView) findViewById(R.id.image);
        roll.setText(rollNo);
        name.setText(studentName);
        if (imagePath == null) {
            image.setImageResource(R.mipmap.profile);
        } else {
            image.setImageBitmap(BitmapFactory.decodeFile(imagePath));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_student_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
