package entities;

import java.io.Serializable;

public class Student implements Serializable {
    public String name;
    public int rollNo;
    public String imagePath;

    public Student() {
    }

    public Student(int rollNo, String name, String imagePath) {
        this.rollNo = rollNo;
        this.name = name;
        this.imagePath = imagePath;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRoll(int rollNo) {
        this.rollNo = rollNo;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getName() {
        return name;
    }

    public int getRoll() {
        return rollNo;
    }

    public String getImagePath() {
        return imagePath;
    }

    @Override
    public String toString() {
        return rollNo + name;
    }
}