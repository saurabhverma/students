package util;

public class AppConstant {
    public static final int RESULT_REQUEST_FOR_SAVE= 2;
    public static final int RESULT_REQUEST_FOR_UPDATE = 1;
    public static final int VIEW = 1;
    public static final int EDIT = 2;
    public static final int DELETE = 3;
    public static final int ASYNCTASK = 0;
    public static final int SERVICE = 1;
    public static final int INTENTSERVICE = 2;

}

